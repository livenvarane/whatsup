//
//  WhatsUp version 1.0 by Liven
//    Follow me: u/LVN_N, @LivenOff
//    If you use anything of my tweak, please send me a private message on my twitter (@LivenOff) before publishing it.
//

%hook NCToggleControl

-(void)setExpanded:(BOOL)arg1 {
	
	NSDictionary *userValues = [[NSUserDefaults standardUserDefaults] persistentDomainForName:@"com.liven.wuprefs"];

	id isAutoClearTextActivated = [userValues valueForKey:@"isAutoClearTextActivated"];

	if([isAutoClearTextActivated isEqual:@true]) {
		%orig(true);
	}
	else {
		%orig();
	}

}

-(void)setTitle:(NSString *)arg1 {

	NSDictionary *userValues = [[NSUserDefaults standardUserDefaults] persistentDomainForName:@"com.liven.wuprefs"];

	NSString *CustomClear = [userValues valueForKey:@"customClearText"];

	if([CustomClear length] == 0) {
		%orig();
	}
	else {
		%orig(CustomClear);
	}

}

%end

%hook NCNotificationListHeaderTitleView

-(void)setTitle:(NSString *)arg1 {

	NSDictionary *userValues = [[NSUserDefaults standardUserDefaults] persistentDomainForName:@"com.liven.wuprefs"];
	NSString *CustomNCTitle = [userValues valueForKey:@"customNCTitle"];

	if([CustomNCTitle length] == 0) {
		%orig();
	}
	else {
		%orig(CustomNCTitle);
	}
}

%end