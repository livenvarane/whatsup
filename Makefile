ARCHS = arm64

INSTALL_TARGET_PROCESSES = SpringBoard

include $(THEOS)/makefiles/common.mk

TWEAK_NAME = WhatsUp

WhatsUp_FILES = Tweak.x
WhatsUp_CFLAGS = -fobjc-arc

include $(THEOS_MAKE_PATH)/tweak.mk
SUBPROJECTS += wuprefs
include $(THEOS_MAKE_PATH)/aggregate.mk
