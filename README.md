# WhatsUp #

Modify some cool things in the notification center.

### Compatible devices/versions ###

* iOS 12.x-13.x
* Tested on checkra1n 13.5 (must work with unc0ver)

### Problems ? Glitchs ? ###

Contact me:

* @LivenOff (Twitter)
* u/LVN_N (Reddit)